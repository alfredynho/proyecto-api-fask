
## APIS REST USERS

_Proyecto Apis Flask_

## Comenzando 🚀


**alfredynho** alfredynho

Alfredo Callizaya Gutierrez
73053045
@alfredynho


### Tecnologías 📋

  * [flask](https://flask.palletsprojects.com/en/2.3.x/)
  * [PosgreSQL](https://www.postgresql.org/)

# Crear Base de Datos en postgres

  - `sudo su postgres`
  - `psql -c "DROP DATABASE api_app"`
  - `psql -c "DROP USER api_user"`
  - `psql -c "CREATE USER api_user WITH NOCREATEDB ENCRYPTED PASSWORD 'XXXYYYZZZ'"`
  - `psql -c "CREATE DATABASE api_app WITH OWNER api_user"`

crear la tabla 


CREATE TABLE IF NOT EXISTS usuario (
  id SERIAL PRIMARY KEY NOT NULL,
  cedula_identidad VARCHAR(255) NOT NULL,
  nombre VARCHAR(255) NOT NULL,
  primer_apellido VARCHAR(255) NOT NULL,
  segundo_apellido VARCHAR(255) NOT NULL,
  fecha_nacimiento DATE
);


la api fue probada con el rest clien, anexo se encuentra el archivo request.http


Proyecto API REST [@alfredynho](alfredynho.cg@gmail.com).


### Instalación 🔧

_Makefile_

Crear el entorno virtual en windows
python -m virtualenv env

activarlo 
cd env/scripts
activate

Crear el entorno virtual en windows
virtualenv env
activarlo

source/env/activate

_opcional_

Instala la extension web para ver mejor las apis
https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa
